### Not in use anymore ! Newer coupling service contianers are based on acc images !!

This repository contains the base Docker image for the lhc-coupling-analysis-service (https://gitlab.cern.ch/coupling/lhc-coupling-analysis-service).

## Alpine linux

grpcio-tools doesn't compile on Alpine Linux (https://github.com/grpc/grpc/issues/8899).

```
apk update
apk add python2-dev python3-dev py2-pip build-base

// NumPy requirement (Alpine doesn't use glibc)
ln -s /usr/include/locale.h /usr/include/xlocale.h
```
