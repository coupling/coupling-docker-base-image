FROM debian:stretch

RUN apt update && apt install -y git vim libblas-dev liblapack-dev gfortran procps htop tcpdump
RUN apt install -y python-dev python-numpy python-pandas python-scipy \
 python3-dev python3-pip python3-urllib3
RUN python3 -m pip install grpcio==1.12.1 grpcio-tools==1.12.1 protobuf==3.5.2.post1
